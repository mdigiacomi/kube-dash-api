from flask import Flask, Response
from flask_restplus import Api, Resource, fields

from kubernetes import client, config



app = Flask(__name__)

api = Api(app, version='1.0', title='kube-dash-api',
    description='API for Kubernetes Dashboard',
)

ns = api.namespace('system', description='System Info')


@ns.route('/')
class System(Resource):
    def get(self):
        config.load_kube_config()
        v1 = client.CoreV1Api()
        print("Getting Pod Info")
        # ret = v1.list_pod_for_all_namespaces(watch=False)
        ret = v1.list_node_with_http_info()
        print(ret)
        return str(ret), 200


if __name__ == '__main__':
    app.run(debug=True)
